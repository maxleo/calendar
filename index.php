<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8' />
    <link href='./assets/css/core/main.min.css' rel='stylesheet' />
    <link href='./assets/css/daygrid/main.min.css' rel='stylesheet' />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="stylesheet" href="./assets/css/personalizado.css">

    <script src='./assets/js/core/main.min.js'></script>
    <script src='./assets/js/interaction/main.min.js'></script>
    <script src='./assets/js/daygrid/main.min.js'></script>
    <script src='./assets/js/core/locales/pt-br.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script src="./assets/js/personalizado.js"></script>

</head>

<body>
    <?php
    if (isset($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    ?>
    <div id='calendar'></div>

    <!-- Modal visualisar / editar -->
    <div class="modal fade" id="vizualizar" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Vizualizar eventos</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="view-event">
                        <dl class="row">
                            <dt class="col-sm-3">ID Evento</dt>
                            <dd class="col-sm-9" id="id"></dd>

                            <dt class="col-sm-3">Titulo do evento</dt>
                            <dd class="col-sm-9" id="title"></dd>

                            <dt class="col-sm-3">Inicio do evento</dt>
                            <dd class="col-sm-9" id="start"></dd>

                            <dt class="col-sm-3">Fim do evento</dt>
                            <dd class="col-sm-9" id="end"></dd>

                        </dl>

                        <button type="button" class="btn btn-warning btn-cancel-vis">Editar</button>
                        <a href="" id="apagar_evento" class="btn btn-danger btn-cancel-vis">Apagar</a>
                    </div>

                    <div class="formEdit">
                        <span id="msg-edit"></span>
                        <form id="EditEvent" method="POST" enctype="multipart/form-data">
                            
                            <input type="hidden" name="id" class="form-control" id="id" required>
                            

                            <div class="row mb-3">
                                <label for="title" class="col-sm-2 col-form-label">Titulo</label>
                                <div class="col-sm-10">
                                    <input type="text" name="title" class="form-control" id="title" placeholder="Titulo do evento" required>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="color" class="col-sm-2 col-form-label">Cor</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <select class="form-control" name="color" id="color">
                                            <option value="">Selecione</option>
                                            <option style="color:#FFD700" value="#FFD700">Amarelo</option>
                                            <option style="color:#0071c5" value="#0071c5">Azul Turquesa</option>
                                            <option style="color:#FF4500" value="#FF4500">Laranja</option>
                                            <option style="color:#8B4513" value="#8B4513">Marrom</option>
                                            <option style="color:#1c1c1c" value="#1C1C1C">Preto</option>
                                            <option style="color:#436EEE" value="#436EEE">Royal Blue</option>
                                            <option style="color:#A020F0" value="#A020F0">Roxo</option>
                                            <option style="color:#436EEE" value="#436EEE">Royal Blue</option>
                                            <option style="color:#40E0D0" value="#40E0D0">Turquesa</option>
                                            <option style="color:#228B22" value="#228B22">Verde</option>
                                            <option style="color:#8B0000" value="#8B0000">Vermelho</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="start" class="col-sm-2 col-form-label">Início do evento</label>
                                <div class="col-sm-10">
                                    <input type="text" name="start" class="form-control" id="start" onkeypress="DataHora(event, this)">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="end" class="col-sm-2 col-form-label">Final do evento</label>
                                <div class="col-sm-10">
                                    <input type="text" name="end" class="form-control" id="end" onkeypress="DataHora(event, this)">
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary btn-cancel-edit me-2">Cancelar</button>

                            <button class="btn btn-success" type="submit" name="cadEvent" id="cadEvent" value="cadEvent">Editar</button>

                        </form>
                    </div>


                </div>

            </div>
        </div>
    </div>

    <!-- Modal cadastrar -->
    <div class="modal fade" id="cadastrar" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Cadastrar eventos</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <span id="msg-cad"></span>
                    <form id="addEvent" method="POST" enctype="multipart/form-data">
                        <div class="row mb-3">
                            <label for="title" class="col-sm-2 col-form-label">Titulo</label>
                            <div class="col-sm-10">
                                <input type="text" name="title" class="form-control" id="title" placeholder="Titulo do evento" required>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="color" class="col-sm-2 col-form-label">Cor</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <select class="form-control" name="color" id="color">
                                        <option value="">Selecione</option>
                                        <option style="color:#FFD700" value="#FFD700">Amarelo</option>
                                        <option style="color:#0071c5" value="#0071c5">Azul Turquesa</option>
                                        <option style="color:#FF4500" value="#FF4500">Laranja</option>
                                        <option style="color:#8B4513" value="#8B4513">Marrom</option>
                                        <option style="color:#1c1c1c" value="#1C1C1C">Preto</option>
                                        <option style="color:#436EEE" value="#436EEE">Royal Blue</option>
                                        <option style="color:#A020F0" value="#A020F0">Roxo</option>
                                        <option style="color:#436EEE" value="#436EEE">Royal Blue</option>
                                        <option style="color:#40E0D0" value="#40E0D0">Turquesa</option>
                                        <option style="color:#228B22" value="#228B22">Verde</option>
                                        <option style="color:#8B0000" value="#8B0000">Vermelho</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="start" class="col-sm-2 col-form-label">Início do evento</label>
                            <div class="col-sm-10">
                                <input type="text" name="start" class="form-control" id="start" onkeypress="DataHora(event, this)">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="end" class="col-sm-2 col-form-label">Final do evento</label>
                            <div class="col-sm-10">
                                <input type="text" name="end" class="form-control" id="end" onkeypress="DataHora(event, this)">
                            </div>
                        </div>

                        <button class="btn btn-success" type="submit" name="cadEvent" id="cadEvent" value="cadEvent">Cadastrar</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</body>

</html>