document.addEventListener('DOMContentLoaded', function () {
    var calendarEl = document.getElementById('calendar');
    var myModalVisualizar = new bootstrap.Modal(document.getElementById('vizualizar'), {
        keyboard: false
    });

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid'],
        //defaultDate: '2019-04-12',
        editable: true,
        locale: "pt-br",
        eventLimit: true, // allow "more" link when too many events
        events: "listEvents.php",
        extraParams: function () {
            return {
                cachebuster: new Date().valueOf()
            };
        },
        eventClick: function (info) {
            //$("#apagar_evento").attr('href', 'apagar_evento.php?id='+info.event.id);
            document.querySelector("#apagar_evento").addEventListener("click", function(){
                if(confirm("entrou")){
                   
                    $.ajax({
                        method: "GET",
                        url: "apagar_evento.php",
                        data: "id="+ info.event.id,
                        contentType: false,
                        processData:false,
                        success: function(retorna){
                            if(retorna['sit']){
                                //$("#msg-cad").html(retorna['msg']);
                                // location.reload();
                                calendar.refetchEvents()
                                myModalVisualizar.hide();
                            }else{
                                $("#msg-edit").html(retorna['msg']);
                            }
                        }
                    })
                }
            });
            
            document.querySelector('#vizualizar #id').innerHTML = info.event.id;
            document.querySelector('#vizualizar #title').innerHTML = info.event.title;
            document.querySelector('#vizualizar #start').innerHTML = info.event.start.toLocaleString();
            document.querySelector('#vizualizar #end').innerHTML = info.event.end.toLocaleString();
            
            document.querySelector('#EditEvent #id').value = info.event.id;
            document.querySelector('#EditEvent #title').value = info.event.title;
            document.querySelector('#EditEvent #color').value = info.event.backgroundColor;
            document.querySelector('#EditEvent #start').value = info.event.start.toLocaleString();
            document.querySelector('#EditEvent #end').value = info.event.end.toLocaleString();
            info.jsEvent.preventDefault();
                       
            
            myModalVisualizar.show();
        },
        selectable: true,
        select: function(info) {
            //alert('Inicio do evento: ' + info.start.toLocaleString() );
            var myModal = new bootstrap.Modal(document.getElementById('cadastrar'), {
                keyboard: false
            });
            document.querySelector("#cadastrar #start").value = info.start.toLocaleString();
            document.querySelector("#cadastrar #end").value = info.end.toLocaleString();
            myModal.show();

        },
    });

    calendar.render();

    document.querySelector("#EditEvent").addEventListener("submit", function(event){
        event.preventDefault();

        $.ajax({
            method: "POST",
            url: "edit_event.php",
            data: new FormData(this),
            contentType: false,
            processData:false,
            success: function(retorna){
                if(retorna['sit']){
                    //$("#msg-cad").html(retorna['msg']);
                    // location.reload();
                    calendar.refetchEvents()
                    myModalVisualizar.hide();
                }else{
                    $("#msg-edit").html(retorna['msg']);
                }
            }
        })

    });
});

function DataHora(evento, objeto){
    var keypress = (window.event) ? event.keyCode : evento.which;
    campo = eval(objeto);
    if(campo.value == '00/00/0000 00:00:00'){
        campo.value = "";
    }

    caracteres = '0123456789';
    separacao1 = '/';
    separacao2 = ' ';
    separacao3 = ':';
    conjunto1 = 2;
    conjunto2 = 5;
    conjunto3 = 10;
    conjunto4 = 13;
    conjunto5 = 16;
    if((caracteres.search(String.fromCharCode(keypress)) != -1) && campo.value.length < (19)){
        if(campo.value.length == conjunto1){
            campo.value = campo.value + separacao1
        } else if(campo.value.length == conjunto2){
            campo.value = campo.value + separacao1
        } else if(campo.value.length == conjunto3){
            campo.value = campo.value + separacao2
        } else if(campo.value.length == conjunto4){
            campo.value = campo.value + separacao3
        } else if(campo.value.length == conjunto5){
            campo.value = campo.value + separacao3
        }
    } else {
        event.returnValue = false
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    document.querySelector("#addEvent").addEventListener("submit", function(event){
        event.preventDefault();

        $.ajax({
            method: "POST",
            url: "cad_event.php",
            data: new FormData(this),
            contentType: false,
            processData:false,
            success: function(retorna){
                if(retorna['sit']){
                    //$("#msg-cad").html(retorna['msg']);
                    location.reload();
                }else{
                    $("#msg-cad").html(retorna['msg']);
                }
            }
        })

    });

    document.querySelector(".btn-cancel-vis").addEventListener("click", function(){
        // let el = document.querySelector('.view-event');
        // let form = document.querySelector('.formEdit');
        // el.style.cssText = "display: none";
        // form.style.cssText = "display: block";

        $('.view-event').slideToggle();
        $('.formEdit').slideToggle();

    });

    document.querySelector(".btn-cancel-edit").addEventListener("click", function(){
        
        $('.formEdit').slideToggle();
        $('.view-event').slideToggle();

    });

    

});
